package com.mikeziray.listview;

import android.app.ListFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * ListView
 * Created by michael.ziray on 9/24/13.
 * Z Studio Labs, LLC. All rights reserved.
 */
public class CardsListFragment extends ListFragment
{
    private ArrayList< Card > cardArrayList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Setup ArrayList
        cardArrayList = new ArrayList<Card>();

        Card card1 = new Card();
        card1.setmQuestion("1 + 1 = ?");
        card1.setmAnswer("2");

        Card card2 = new Card();
        card2.setmQuestion("2 + 2 = ?");
        card2.setmAnswer("4");

        cardArrayList.add( card1 );
        cardArrayList.add( card2 );

        // Setup adapter
        ArrayAdapter<Card> arrayAdapter = new ArrayAdapter<Card>(getActivity(), android.R.layout.simple_list_item_1, cardArrayList);
        setListAdapter( arrayAdapter );
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onListItemClick( ListView l, View v, int position, long id)
    {
        Card currentCard = (Card)getListAdapter().getItem( position );

        Toast.makeText( getActivity(), "Answer: " + currentCard.getmAnswer(), Toast.LENGTH_SHORT ).show();
    }
}


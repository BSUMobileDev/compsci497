package com.mikeziray.listview;

import android.app.Activity;
import android.app.FragmentManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentManager fragmentManager = getFragmentManager();

        CardsListFragment fragment = (CardsListFragment)fragmentManager.findFragmentById(R.id.fragmentContainer);

        if( fragment == null)
        {
            fragment = new CardsListFragment();

            fragmentManager.beginTransaction().add( R.id.fragmentContainer, fragment ).commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem)
    {
        switch ( menuItem.getItemId() )
        {
            case R.id.action_addcard:
                Toast.makeText( this, "Add Card selected", Toast.LENGTH_LONG).show();
             default:
                 Toast.makeText( this, "Something else selected", Toast.LENGTH_SHORT).show();
        }
        return true;
    }
}

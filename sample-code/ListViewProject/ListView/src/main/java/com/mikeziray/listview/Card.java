package com.mikeziray.listview;

/**
 * ListView
 * Created by michael.ziray on 9/24/13.
 * Z Studio Labs, LLC. All rights reserved.
 */
public class Card {

    public Card() {
        this.mQuestion = "Default Question";
        this.mAnswer   = "Default Answer";
    }

    public String getmQuestion() {
        return mQuestion;
    }

    public void setmQuestion(String mQuestion) {
        this.mQuestion = mQuestion;
    }

    public String getmAnswer() {
        return mAnswer;
    }

    public void setmAnswer(String mAnswer) {
        this.mAnswer = mAnswer;
    }

    @Override
    public String toString()
    {
        return this.mQuestion;
    }

    private String mQuestion;
    private String mAnswer;
}

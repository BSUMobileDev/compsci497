package com.mikeziray.fragmentproject;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentManager fragmentManager = getFragmentManager();

        Fragment textFragment = fragmentManager.findFragmentById(R.id.fragmentContainer);

        if( textFragment == null)
        {
            textFragment = new SomeFragment();

            fragmentManager.beginTransaction().add(R.id.fragmentContainer, textFragment).commit();
        }
    }


    
}

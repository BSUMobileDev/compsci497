package com.mikeziray.fragmentproject;

/**
 * FragmentProject
 * Created by michael.ziray on 10/3/13.
 * Z Studio Labs, LLC. All rights reserved.
 */
public class CardsController {
    private static CardsController ourInstance = new CardsController();

    public static CardsController getInstance() {
        return ourInstance;
    }

    private CardsController() {
    }
}

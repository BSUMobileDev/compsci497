package com.mikeziray.fragmentproject;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class SomeFragment extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState)
    {
        View textFragmentView = inflater.inflate(R.layout.fragment_some, parent, false);

        TextView helloTextView = (TextView)textFragmentView.findViewById(R.id.fragButton);


        return textFragmentView;
    }
    
}

package com.mikeziray.googlemaps;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MainActivity extends FragmentActivity {

    private GoogleMap mGoogleMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        int isEnabled = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (isEnabled != ConnectionResult.SUCCESS) {
            GooglePlayServicesUtil.getErrorDialog(isEnabled, this, 0);

            if( isEnabled == ConnectionResult.SERVICE_MISSING )
            {
                Toast.makeText(this, "Service Missing", Toast.LENGTH_LONG).show();
            }
            else if( isEnabled == ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED )
            {
                Toast.makeText(this, "Service update required", Toast.LENGTH_LONG).show();
            }
            else if( isEnabled == ConnectionResult.SERVICE_DISABLED )
            {
                Toast.makeText(this, "Service Disabled", Toast.LENGTH_LONG).show();
            }
        }
        else
        {
            SupportMapFragment mapFragment = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_container));
            mGoogleMap = mapFragment.getMap();

//            mGoogleMap.setMyLocationEnabled(true);

            mGoogleMap.setOnMarkerClickListener( new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {
                    Toast.makeText( getApplicationContext(), marker.getTitle(), Toast.LENGTH_LONG ).show();
                    return false;
                }
            });

            // Map types
            mGoogleMap.setMapType( GoogleMap.MAP_TYPE_NORMAL );
            mGoogleMap.setMapType( GoogleMap.MAP_TYPE_TERRAIN );
            mGoogleMap.setMapType( GoogleMap.MAP_TYPE_SATELLITE );
            mGoogleMap.setMapType( GoogleMap.MAP_TYPE_HYBRID );

            LatLng coordinate = new LatLng( 43.60352, -116.20141 );
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position( coordinate );
            markerOptions.title( "Building Name" );
            markerOptions.snippet( "Description" );

            // ROSE color icon
            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE));

            // GREEN color icon
            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));

            // Drawable icon
            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_launcher));

            mGoogleMap.setOnInfoWindowClickListener( new GoogleMap.OnInfoWindowClickListener() {
                @Override
                public void onInfoWindowClick(Marker marker) {
                    Toast.makeText( getApplicationContext(), "Info window!", Toast.LENGTH_LONG ).show();
                }
            });

            // Finally, add the marker to the map
            mGoogleMap.addMarker(markerOptions);

            // Move the camera instantly to building with a zoom of 15.
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(coordinate, 15));

            // Zoom in, animating the camera.
            mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
}

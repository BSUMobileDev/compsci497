package com.mikeziray.bsumaps;

import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.util.AttributeSet;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.android.gms.maps.SupportMapFragment;

public class MainActivity extends ActionBarActivity
{
    private ViewPager mViewPager;

    private final static int NUMBER_OF_TABS = 2;

    private final static int TAB_LISTVIEW   = 0;
    private final static int TAB_MAP        = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null)
        {
            mViewPager = new NoScrollViewPager( this );
            mViewPager.setId(R.id.viewPager);
            setContentView(mViewPager);

            setupActionBar();

            setupViewPager();
        }
    }


    private void setupActionBar()
    {
        ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        ActionBar.TabListener tabListener = new ActionBar.TabListener() {
            public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft)
            {
                mViewPager.setCurrentItem(tab.getPosition());
            }
            public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {}
            public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {}
        };

        ActionBar.TabListener mapTabListener = new ActionBar.TabListener() {
            public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft)
            {
                mViewPager.setCurrentItem(tab.getPosition());
            }
            public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {}
            public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {}
        };

        actionBar.addTab(actionBar.newTab().setText(getString(R.string.frag_list_view_name)).setTabListener(tabListener));
        actionBar.addTab(actionBar.newTab().setText(getString(R.string.frag_map_view_name)).setTabListener(mapTabListener));
    }


    private void setupViewPager()
    {
        FragmentManager fragmentManager = getSupportFragmentManager();
        mViewPager.setAdapter( new FragmentStatePagerAdapter( fragmentManager )
        {
            @Override public int getCount()
            {
                return NUMBER_OF_TABS;
            }

            @Override public Fragment getItem( int pagerPosition )
            {
                Fragment fragmentToReturn;
                switch ( pagerPosition )
                {
                    case TAB_LISTVIEW:
                        fragmentToReturn = new ListViewFragment();
                        break;

                    case TAB_MAP:
//                        fragmentToReturn = new GoogleMapFragment();
                        fragmentToReturn = SupportMapFragment.newInstance();

                        break;

                    default:
                        fragmentToReturn = new Fragment();
                }

                return fragmentToReturn;
            }
        });

        mViewPager.setOnPageChangeListener( new ViewPager.OnPageChangeListener()
        {
            public void onPageScrollStateChanged( int state){}
            public void onPageScrolled( int pos, float posOffset, int posOffsetPixels){}
            public void onPageSelected( int pos){}
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    public class NoScrollViewPager extends ViewPager {

        public NoScrollViewPager (Context context) {
            super(context);
        }

        public NoScrollViewPager (Context context, AttributeSet attrs) {
            super(context, attrs);
        }

        @Override
        protected boolean canScroll(View v, boolean checkV, int dx, int x, int y) {
            return true;
        }

    }

}

package com.mikeziray.bsumaps;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * BSUMaps
 * Created by michael.ziray on 10/25/13.
 * Z Studio Labs, LLC. All rights reserved.
 */
public class ListViewFragment extends ListFragment
{
    private ArrayList<Building> mBuildings;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().setTitle(R.string.frag_list_view_name);

        mBuildings = BuildingsController.getBuildings();

        BuildingsListAdapter adapter = new BuildingsListAdapter( mBuildings );
        setListAdapter(adapter);
    }

    public void onListItemClick(ListView l, View v, int position, long id)
    {
        Intent mapActivityIntent = new Intent( this.getActivity(), MapActivity.class );
        mapActivityIntent.putExtra("MAP_ID", 0);
        startActivity( mapActivityIntent );
    }

    private class BuildingsListAdapter extends ArrayAdapter<Building>
    {
        public BuildingsListAdapter(ArrayList<Building> buildings)
        {
            super(getActivity(), android.R.layout.simple_list_item_1, buildings);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            // if we weren't given a view, inflate one
            if (null == convertView) {
                convertView = getActivity().getLayoutInflater()
                        .inflate(R.layout.list_item_building, null);
            }

            // configure the view
            Building currentBuilding = getItem(position);

            TextView nameTextView = (TextView)convertView.findViewById(R.id.building_name);
            nameTextView.setText( currentBuilding.getName() );

            TextView positionTextView = (TextView)convertView.findViewById(R.id.building_position);
            positionTextView.setText( currentBuilding.getPosition().toString() );

            return convertView;
        }
    }
}

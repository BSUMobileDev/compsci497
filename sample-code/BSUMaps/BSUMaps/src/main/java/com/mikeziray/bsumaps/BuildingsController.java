package com.mikeziray.bsumaps;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

/**
 * BSUMaps
 * Created by michael.ziray on 10/24/13.
 * Z Studio Labs, LLC. All rights reserved.
 */
public class BuildingsController
{
    private static BuildingsController ourInstance = new BuildingsController();
    private static ArrayList<Building> mBuildings;


    public static ArrayList<Building> getBuildings()
    {
        return mBuildings;
    }


    public static BuildingsController getInstance()
    {
        return ourInstance;
    }


    private BuildingsController()
    {
        mBuildings = new ArrayList<Building>();

        loadBuildings();
    }


    private void loadBuildings()
    {
        Building newBuilding;

        // Get this from the web instead
        for(int index = 0; index < 5; index++)
        {
            newBuilding = new Building();
            newBuilding.setName("Building " + index);
            double offset = 5 * index;
            newBuilding.setPosition( new LatLng(-116.123 + offset, 28.1111 + offset) );
            mBuildings.add( newBuilding );
        }
    }
}

package com.mikeziray.bsumaps;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;

import java.util.ArrayList;

/**
 * BSUMaps
 * Created by michael.ziray on 10/25/13.
 * Z Studio Labs, LLC. All rights reserved.
 */
public class GoogleMapFragment extends SupportMapFragment
{
    private ArrayList<Building> mBuildings;

    GoogleMap mGoogleMap;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        getActivity().setTitle(R.string.frag_list_view_name);

        setRetainInstance(true);

        mBuildings = BuildingsController.getBuildings();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = super.onCreateView(inflater, container, savedInstanceState );

        mGoogleMap = getMap();

        MarkerOptions markerOptions;
        Building currentBuilding;

        for(int markerIndex = 0; markerIndex < mBuildings.size(); markerIndex++)
        {
            currentBuilding = mBuildings.get(markerIndex);
            markerOptions = new MarkerOptions();

            LatLng coordinate = new LatLng( currentBuilding.getPosition().latitude, currentBuilding.getPosition().longitude );
            markerOptions.position( coordinate );
            markerOptions.title( currentBuilding.getName() );
            markerOptions.snippet( currentBuilding.getDescription() );
            mGoogleMap.addMarker(markerOptions);
        }


        PolygonOptions cityBoundaryPolygon = new PolygonOptions();
        double [] boundary = BoiseLimits.getBoundary();
        for(int index = 0; index < boundary.length; ){
            double longitude = boundary[index++];
            double latitude  = boundary[index++];
            cityBoundaryPolygon.add(new LatLng(latitude, longitude ));
        }
        cityBoundaryPolygon.strokeWidth(5);
        cityBoundaryPolygon.fillColor(Color.RED);
        cityBoundaryPolygon.geodesic(true);
        Polygon polygon = mGoogleMap.addPolygon(cityBoundaryPolygon);

        return view;
    }

}

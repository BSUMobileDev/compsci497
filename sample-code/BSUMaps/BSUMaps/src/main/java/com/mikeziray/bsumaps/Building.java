package com.mikeziray.bsumaps;

import com.google.android.gms.maps.model.LatLng;

/**
 * BSUMaps
 * Created by michael.ziray on 10/24/13.
 * Z Studio Labs, LLC. All rights reserved.
 */
public class Building {
    private String name;
    private String description;
    private LatLng position;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LatLng getPosition() {
        return position;
    }

    public void setPosition(LatLng position) {
        this.position = position;
    }
}

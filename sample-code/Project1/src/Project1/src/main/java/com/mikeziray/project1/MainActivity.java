package com.mikeziray.project1;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity {

    private Button mSetNameButton;
    private Uri fileUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Get reference to Set Name button
        mSetNameButton = (Button)findViewById(R.id.setNameButton);

        // Set name button will create an intent, load it with data (extras) and launch a new activity
        mSetNameButton.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // We're intending to create a ChildActivity Activity, from this activity
                Intent newIntent = new Intent( MainActivity.this, ChildActivity.class );

                // Load the intent with data to pass to child activity
                newIntent.putExtra( ChildActivity.FIRST_NAME, "Mike");
                newIntent.putExtra( ChildActivity.LAST_NAME,  "Ziray");

//                Start Activity
//                startActivity( newIntent ); // We'd use this if we don't need a direct result from child activity
                startActivityForResult( newIntent, 0 ); // Announce your intent to start a new activity, ID is 0
            }
        });

    }

    @Override
    protected void onActivityResult( int requestCode, int resultCode, Intent data)
    {
        // Just make sure the child passed something back
        if( data == null ){return;}

        // Get values from the extras bundle that was passed back from child activity
        Bundle extras = data.getExtras();
        String firstName = extras.getString( ChildActivity.FIRST_NAME );
        String lastName  = extras.getString( ChildActivity.LAST_NAME );
        String wholeName = extras.getString( ChildActivity.WHOLE_NAME );

        // Get local instance of a textview that exists in the layout
        TextView helloTextView = (TextView)findViewById(R.id.hello_text);

        // Output the values
        helloTextView.setText("Your first name is " + firstName + ", your last name is " + lastName + ", and your whole name is " + wholeName);

    }
}

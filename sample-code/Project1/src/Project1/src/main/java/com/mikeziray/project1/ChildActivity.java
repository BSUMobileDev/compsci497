package com.mikeziray.project1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ChildActivity extends Activity {

    // Static constants for mapping our data to and from intents
    public static final String FIRST_NAME = "FIRST_NAME";
    public static final String LAST_NAME  = "LAST_NAME";
    public static final String WHOLE_NAME = "WHOLE_NAME";

    // First name, last name editable text views
    private TextView mFirstNameTextView;
    private TextView mLastNameTextView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_child);

        // Get the intent that this activity was created for
        Intent parentIntent = getIntent();

        // Get the extras bundle out of the intent
        Bundle extras = parentIntent.getExtras();

        // Get the first and last name variables out of the extras hash table
        String firstName = extras.getString(FIRST_NAME);
        String lastName  = extras.getString(LAST_NAME);

        // Get references to the text views so we can manipulate them in code
        mFirstNameTextView = (TextView)findViewById(R.id.firstNameTextView);
        mLastNameTextView  = (TextView)findViewById(R.id.lastNameTextView);

        // Once we have their references, we can set their display text
        mFirstNameTextView.setText(firstName);
        mLastNameTextView.setText(lastName);

        // Get the save button's reference to add a click listener to it
        Button saveButton = (Button)findViewById(R.id.button);

        // On tap (click), create a new intent to send info back to parent activity
        saveButton.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Create brand new intent
                Intent resultsIntent = new Intent();

                // Assign variables
                resultsIntent.putExtra(FIRST_NAME, mFirstNameTextView.getText().toString());
                resultsIntent.putExtra(LAST_NAME, mLastNameTextView.getText().toString());

                // Send back a calculated value
                String wholeName = mFirstNameTextView.getText().toString() + " " + mLastNameTextView.getText().toString();
                resultsIntent.putExtra(WHOLE_NAME, wholeName);

                // Sets the intent to send back to parent, with OK status
                setResult( RESULT_OK, resultsIntent );
                //setResult( RESULT_CANCELED, resultsIntent ); // If cancelled

                // This tells the child activity to finish and the Activity Manager will pop it off the stack
                finish();

            }
        });
    }
}

package com.mikeziray.webservices;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class MainActivity extends Activity {


    final String resourceURIString        = "http://zstudiolabs.com/labs/compsci497/buildings.json";

    private Button mLoadResource;
    private GetResourceTask mGetResourceTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mLoadResource = (Button)findViewById( R.id.load_resource_button );

        mLoadResource.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mGetResourceTask = new GetResourceTask( resourceURIString ); // Create a
                mGetResourceTask.execute(new String[] { "", "Other stuff"});
            }
        });
    }

    public class GetResourceTask extends AsyncTask<String, Void, Boolean> {

        private int resultCount;
        private String hostURL;

        public GetResourceTask(String host) {
            this.hostURL = host;
        }

        @SuppressLint("NewApi")
        @Override
        protected Boolean doInBackground(String... params) {
            int indexCounter = 0;
            try {
//                String url = resourceURIString + actionString  + "?q=" + URLEncoder.encode(params[0], "UTF-8"); // Always encode URL parameters or other user input
                String url = resourceURIString + params[0];
                HttpParams httpParams = new BasicHttpParams();

                // Set 30 second timeouts
                HttpConnectionParams.setConnectionTimeout(httpParams, 30000);
                HttpConnectionParams.setSoTimeout(httpParams, 30000);

                // Create HTTP client to load resource
                DefaultHttpClient httpClient = new DefaultHttpClient(httpParams);

//            String byteData;
//            if (Build.VERSION.SDK_INT >= 8)
//            {
//                byteData = Base64.encodeToString(credentials.getBytes(), Base64.URL_SAFE | Base64.NO_WRAP);
//            }
//            else
//            {
//                byteData = Base64Utility.encodeToString( credentials.getBytes(), Base64Utility.DEFAULT );
//            }

                /* Setup the GET request
                ** Possible methods:
                ** HttpPost
                ** HttpPut
                ** HttpDelete
                ** HttpGet
                */
                HttpGet httpGet = new HttpGet(url);
//                httpGet.setHeader("Accept", "application/json");
                httpGet.setHeader("Content-type", "application/json");
//                httpGet.setHeader("Content-type", "image/png");
//                httpGet.setHeader("AdditionalParameter", "Value");
//            httpGet.setHeader("Authorization", "Basic " + byteData);

                ResponseHandler responseHandler = new BasicResponseHandler();
                String response = (String) httpClient.execute(httpGet, responseHandler);

                if (response != null) {
                    JSONArray buildingsJSONArray = new JSONArray(response);
                    JSONObject buildingJSONObject = buildingsJSONArray.getJSONObject( 0 );
                    String timeString = buildingJSONObject.getString("name");
                    Toast.makeText(getApplicationContext(), timeString, Toast.LENGTH_LONG);

//                    int resultCount = results.length();
//                    for (int i = 0; i < resultCount; i++) {

//                        HashMap<String, String> resultData = new HashMap<String, String>();
//                        resultData.put("ParcelNumber",
//                                resultItem.getString("ParcelNumber"));
//                    }
                }
            }
            catch (ClientProtocolException e) {
                e.printStackTrace();
                return false;
            }
            catch (IOException e) {
                e.printStackTrace();
                return false;
            }
            catch (JSONException e) {
                e.printStackTrace();
                return false;
            }

            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mGetResourceTask = null;
//            resultsAdapter.notifyDataSetChanged();
//            if (success) {
//                if (resultCount != 0) {
//                    Utils.show(mSearchResultView, getActivity());
//                    Utils.hide(mStatusView, getActivity());
//                } else {
//                    Utils.show(mSearchFormView, getActivity());
//                    Utils.hide(mStatusView, getActivity());
//                    new AlertDialog.Builder(getActivity())
//                            .setMessage(
//                                    "There were no resource matching your request")
//                            .setPositiveButton(
//                                    getResources().getString(R.string.ok), null)
//                            .show();
//                }
//            } else {
//                Utils.show(mSearchFormView, getActivity());
//                Utils.hide(mStatusView, getActivity());
//                if (!Utils.isNetworkConnected(getActivity()))
//                    Toast.makeText(getActivity(),
//                            getResources().getString(R.string.no_connection),
//                            Toast.LENGTH_LONG).show();
//            }
        }

        @Override
        protected void onCancelled() {
            mGetResourceTask = null;
//            Utils.show(mSearchFormView, getActivity());
//            Utils.hide(mStatusView, getActivity());
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

}

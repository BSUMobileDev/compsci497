package com.mikeziray.database;

import android.provider.BaseColumns;

/**
 * Database
 * Created by michael.ziray on 10/12/13.
 * Z Studio Labs, LLC. All rights reserved.
 */
public final class LocationsContract
{
    public LocationsContract(){}

    public static abstract class LocationsEntry implements BaseColumns
    {
        /* LOCATIONS TABLE */
        public static final String TABLE_LOCATIONS   = "Locations";
        public static final String COLUMN_LOCATION_NAME = "location_name";
        public static final String COLUMN_LOCATION_ID   = "location_id";
        public static final String COLUMN_DESCRIPTION   = "description";


        /* COORDINATES TABLE */
        public static final String TABLE_COORDINATES = "Coordinates";
        public static final String COLUMN_LATITUDE = "latitude";
        public static final String COLUMN_LONGITUDE = "longitude";
    }
}

package com.mikeziray.database;

/**
 * Database
 * Created by michael.ziray on 10/12/13.
 * Z Studio Labs, LLC. All rights reserved.
 */
public class Location
{
    private int locationID;
    private String locationName;
    private String locationDescription;
    private GPSCoordinate locationCoordinates;

    public int getLocationID() {
        return locationID;
    }

    public void setLocationID(int locationID) {
        this.locationID = locationID;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getLocationDescription() {
        return locationDescription;
    }

    public void setLocationDescription(String locationDescription) {
        this.locationDescription = locationDescription;
    }

    public GPSCoordinate getLocationCoordinates() {
        return locationCoordinates;
    }

    public void setLocationCoordinates(GPSCoordinate locationCoordinates) {
        this.locationCoordinates = locationCoordinates;
    }
}

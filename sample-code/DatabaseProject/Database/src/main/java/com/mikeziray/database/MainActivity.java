package com.mikeziray.database;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;

public class MainActivity extends Activity {

    private DatabaseController databaseController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        databaseController = new DatabaseController( this );


        Location newLocation = new Location();
        newLocation.setLocationName("Library");
        newLocation.setLocationDescription("Library description");

        GPSCoordinate gpsCoordinate = new GPSCoordinate();
        gpsCoordinate.setLatitude( 123.123 );
        gpsCoordinate.setLongitude( -321.321 );
        newLocation.setLocationCoordinates( gpsCoordinate );

        databaseController.addLocation( newLocation );

        databaseController.removeLocation( newLocation );
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
}

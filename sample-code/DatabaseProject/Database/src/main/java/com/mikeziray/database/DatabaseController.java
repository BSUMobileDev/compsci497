package com.mikeziray.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.util.Log;

/**
 * Database
 * Created by michael.ziray on 10/12/13.
 * Z Studio Labs, LLC. All rights reserved.
 */
public class DatabaseController extends SQLiteOpenHelper {

    /* DATABASE INFO */
    private static final String DB_NAME = "android_db";
    private static final int DB_VERSION = 1;


    public DatabaseController( Context context )
    {
        super( context, DB_NAME, null, DB_VERSION );
    }

    @Override
    public void onCreate(SQLiteDatabase database)
    {
        // Create Coordinates Table
        String locationsTableQuery = "CREATE TABLE " + LocationsContract.LocationsEntry.TABLE_COORDINATES+ "("+
                "_id integer primary key autoincrement, " +
                LocationsContract.LocationsEntry.COLUMN_LATITUDE  + " real, " +
                LocationsContract.LocationsEntry.COLUMN_LONGITUDE + " real " +
                ")";
        try{
            database.execSQL( locationsTableQuery );
        }
        catch ( Exception exception )
        {
            Log.e("MZ",exception.getMessage());
        }

        // Create Locations Table
        String coordinatesTableQuery = "CREATE TABLE " + LocationsContract.LocationsEntry.TABLE_LOCATIONS +
                "("+
                "_id integer primary key autoincrement, " +
                LocationsContract.LocationsEntry.COLUMN_LOCATION_NAME + " varchar(255)," +
                LocationsContract.LocationsEntry.COLUMN_LOCATION_ID + " integer references " +
                LocationsContract.LocationsEntry.TABLE_LOCATIONS + "(_id)," +
                LocationsContract.LocationsEntry.COLUMN_DESCRIPTION + " varchar(1023) " +
                ")";

        database.execSQL( coordinatesTableQuery );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        switch ( oldVersion )
        {
            case 1:
                // Adds new table Majors: db version 2
            case 2:
                // Adds new column 'times' to table Majors: db version 3
            case 3:
                // Removes 'times' from table Majors: db version 4
            default:
                // Unhandled migration
                break;
        }
    }


    public void addLocation( Location newLocation )
    {
        AddLocationTask addLocationTask = new AddLocationTask();
        addLocationTask.execute(newLocation);
    }

    private void addLocationAsync( Location newLocation )
    {
        ContentValues contentValues = new ContentValues();
        contentValues.put( LocationsContract.LocationsEntry.COLUMN_LATITUDE,  newLocation.getLocationCoordinates().getLatitude()  );
        contentValues.put( LocationsContract.LocationsEntry.COLUMN_LONGITUDE, newLocation.getLocationCoordinates().getLongitude() );

        long coordinateID = getWritableDatabase().insert( LocationsContract.LocationsEntry.TABLE_COORDINATES, null, contentValues);


        contentValues = new ContentValues();
        contentValues.put( LocationsContract.LocationsEntry.COLUMN_LOCATION_NAME, newLocation.getLocationName() );
        contentValues.put( LocationsContract.LocationsEntry.COLUMN_DESCRIPTION,   newLocation.getLocationDescription() );
        contentValues.put( LocationsContract.LocationsEntry.COLUMN_LOCATION_ID,   coordinateID );
        long locationID = getWritableDatabase().insert( LocationsContract.LocationsEntry.TABLE_LOCATIONS, null, contentValues);
    }


    public void removeLocation( Location locationToRemove )
    {
        RemoveLocationTask removeLocationTask = new RemoveLocationTask();
        removeLocationTask.execute( locationToRemove );
    }

    private void removeLocationAsync( Location locationToRemove )
    {
        String [] whereArguments = { String.valueOf(locationToRemove.getLocationID()) };

        try {
            int delete = getWritableDatabase().delete(LocationsContract.LocationsEntry.TABLE_COORDINATES,
                    LocationsContract.LocationsEntry._ID + "=",
                    whereArguments);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }


    private class AddLocationTask extends AsyncTask<Location, Void, Void>
    {
        @Override
        protected Void doInBackground( Location...parameters )
        {
            for (Location currentLocation : parameters) {
                addLocationAsync(currentLocation);
            }
            return null;
        }
    }


    private class RemoveLocationTask extends AsyncTask<Location, Void, Void>
    {
        @Override
        protected Void doInBackground(Location... parameters)
        {
            for (Location currentLocation : parameters) {
                removeLocationAsync(currentLocation);
            }
            return null;
        }
    }
}

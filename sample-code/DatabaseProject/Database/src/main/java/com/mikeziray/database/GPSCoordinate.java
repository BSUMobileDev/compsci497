package com.mikeziray.database;

/**
 * Database
 * Created by michael.ziray on 10/12/13.
 * Z Studio Labs, LLC. All rights reserved.
 */
public class GPSCoordinate
{
    private int coordinateID;
    private double latitude;
    private double longitude;


    public int getCoordinateID() {
        return coordinateID;
    }

    public void setCoordinateID(int coordinateID) {
        this.coordinateID = coordinateID;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}

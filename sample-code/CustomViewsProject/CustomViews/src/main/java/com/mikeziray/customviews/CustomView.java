package com.mikeziray.customviews;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

/**
 * CustomViews
 * Created by michael.ziray on 10/27/13.
 * Z Studio Labs, LLC. All rights reserved.
 */
public class CustomView extends View
{

    private Paint mTextPaint;


    public CustomView(Context context, AttributeSet attrs) {
        super(context, attrs);

        initLabelView();
    }

    public CustomView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        initLabelView();
    }

    public CustomView(Context context) {
        super(context);

        initLabelView();
    }

    private final void initLabelView() {
        mTextPaint = new Paint();
        mTextPaint.setAntiAlias(true);
        // Must manually scale the desired text size to match screen density
        mTextPaint.setTextSize(16 * getResources().getDisplayMetrics().density);
        mTextPaint.setColor(0xFF000000);
        setPadding(3, 3, 3, 3);
    }

    /**
     * Render the text
     *
     * @see android.view.View#onDraw(android.graphics.Canvas)
     */
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        // Text
        canvas.drawText("Drawing text", 20, 20, mTextPaint);

        // Rect
        RectF rectangle = new RectF(20, 50, 300, 300);
        Paint rectanglePaint = new Paint();
        rectanglePaint.setColor( 0xFF000000 );
        canvas.drawRect( rectangle, rectanglePaint );

        // Line paths
        Paint linePaint = new Paint();
        linePaint.setColor( getResources().getColor(android.R.color.holo_orange_dark) );
        linePaint.setStrokeWidth(3.0f);
        linePaint.setAntiAlias( true );
        float[]pointsArray = new float[]{20,400, 20,480, 50,430, 50,350};
        Path linePath = new Path();
        linePath.moveTo(pointsArray[0],pointsArray[1]);
        for(int index = 2; index < pointsArray.length;)
        {
            // If even number of points x,y
            if( pointsArray.length % 2 == 0 )
            {
                linePath.lineTo(pointsArray[index++],pointsArray[index++]);
            }
        }

        canvas.drawPath(linePath, linePaint);
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(measureWidth(widthMeasureSpec),
                measureHeight(heightMeasureSpec));
    }
    /**
     * Determines the width of this view
     * @param measureSpec A measureSpec packed into an int
     * @return The width of the view, honoring constraints from measureSpec
     */
    private int measureWidth(int measureSpec) {
        int result = 0;
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);

        if (specMode == MeasureSpec.EXACTLY) {
            // We were told how big to be
            result = specSize;
        }
        else {
            // Measure the text
//            result = (int) mTextPaint.measureText(mText) + getPaddingLeft()
//                    + getPaddingRight();
//            if (specMode == MeasureSpec.AT_MOST) {
//                // Respect AT_MOST value if that was what is called for by measureSpec
//                result = Math.min(result, specSize);
//            }
        }

        return result;
    }

    /**
     * Determines the height of this view
     * @param measureSpec A measureSpec packed into an int
     * @return The height of the view, honoring constraints from measureSpec
     */
    private int measureHeight(int measureSpec) {
        int result = 0;
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);

//        mAscent = (int) mTextPaint.ascent();
        if (specMode == MeasureSpec.EXACTLY) {
            // We were told how big to be
            result = specSize;
        }
        else {
            // Measure the text (beware: ascent is a negative number)
//            result = (int) (-mAscent + mTextPaint.descent()) + getPaddingTop()
//                    + getPaddingBottom();
//            if (specMode == MeasureSpec.AT_MOST) {
//                // Respect AT_MOST value if that was what is called for by measureSpec
//                result = Math.min(result, specSize);
//            }
        }
        return result;
    }
}
